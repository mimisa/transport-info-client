// import React from 'react';
// import {View} from 'react-native';
// import getDirections from "react-native-google-maps-directions";
//
// import PropTypes from 'prop-types';
//
// const ASPECT_RATIO = 1;
// const LATITUDE = 37.78825;
// const LONGITUDE = -122.4324;
// const LATITUDE_DELTA = 2.0922;
// const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
// const SPACE = 0.01;
// const MARKER_ID = 'AIzaSyC34IpqMJfQuSp96f05kp9JGI_ZkxnhQwc';
//
// export default class Maps extends React.Component {
//
//     constructor() {
//         super();
//         this.state = {
//             coordinates: []
//         }
//     }
//
//     componentDidMount() {
//         fetch("/line.json").then(function (data) {
//             console.log(data);
//         });
//     }
//
//     render() {
//         return (
//             <View>
//                 <MapView
//                     ref={(ref) => {
//                         this.map = ref;
//                     }}
//                     initialRegion={{
//                         latitude: LATITUDE,
//                         longitude: LONGITUDE,
//                         latitudeDelta: LATITUDE_DELTA,
//                         longitudeDelta: LONGITUDE_DELTA,
//                     }}
//                 >
//                     <MapView.Marker
//                         // key={MARKER_ID.id}
//                         coordinate={{
//                             latitude: LATITUDE + 1,
//                             longitude: LONGITUDE + 1
//                         }}
//                     />
//                 </MapView>
//             </View>
//         )
//     }
// }