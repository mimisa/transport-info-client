import React, {useState, useEffect} from 'react';
import axios from 'axios';
import GoogleMapReact from 'google-map-react';
import Map from 'google-map-react'
import Polyline from 'google-map-react'
import Marker from "./Marker";


const SimpleMap = (props) => {
    const [center, setCenter] = useState({lat: 59.334591, lng: 18.063240});
    const [zoom, setZoom] = useState(12);
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null)

    useEffect(() => {
        setLoading(true)
        setError(null)

        axios.get('http://localhost:8080/traffic-info/BUS/line/56')
           .then(json => {
                setLoading(false)
                if (json.data.lineRoute) {
                    console.log("NOT EMPTY")
                    const data =
                        json.data.lineRoute.stops.map(({locationNorthingCoordinate, locationEastingCoordinate}) =>
                            ({lat: parseFloat(locationNorthingCoordinate), lng: parseFloat(locationEastingCoordinate)}));
                    setData(data)
                } else {
                    console.log("EMPTY")
                    setData([])
                }
            })
            .catch(err => {
                setError(err)
                setLoading(false)
            })
    }, []);

return (
    <div style={{height: '100vh', width: '100%'}}>
        <Map
            bootstrapURLKeys={{key: 'AIzaSyB5VBgA4WJyrEnCB8useGQW3atKqqY_V_4'}}
            defaultCenter={center}
            defaultZoom={zoom}
        >
            {data.map((position) => (
                    <Marker
                        position={position}
                        icon={{
                            url: "/bus.png"
                        }}
                    />
                ))
            }
            {/*<Polyline*/}
            {/*    fillColor="red"*/}
            {/*    fillOpacity={1}*/}
            {/*    path={data}*/}
            {/*    strokeColor="red"*/}
            {/*    strokeOpacity={0.8}*/}
            {/*    strokeWeight={8}*/}
            {/*/>*/}
        </Map>
    </div>
);
}

export default SimpleMap;