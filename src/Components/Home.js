import React, {useEffect, useState} from 'react';
import axios from "axios";

const Home = () => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null)

    useEffect(() => {
        setLoading(true)
        setError(null)

        axios.get('http://localhost:8080/traffic-info/BUS/line/56')
            .then(response => {
                console.log(response)

                return response;
            })
            .then(json => {
                setLoading(false)
                console.log(json);

                if (json.data.lineRoute) {
                    console.log("NOT EMPTY")
                    setData(json.data.lineRoute.stops)
                } else {
                    console.log("EMPTY")
                    setData([])
                }
            })
            .catch(err => {
                setError(err)
                setLoading(false)
            })
    }, []);
    return (
        <div>HOME

        </div>
    )
}

export default Home;