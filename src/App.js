import React from 'react';

import './App.css';
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import Home from "./Components/Home";
import SimpleMap from "./Components/SimpleMap";

const App = () => {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/map" component={SimpleMap}/>
                </Switch>
            </Router>
        < /div>
    );
}

export default App;
