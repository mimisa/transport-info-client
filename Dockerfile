FROM node:13-alpine as builder

RUN mkdir -p /app

WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

COPY package*.json ./

run npm install

EXPOSE 80

CMD [ "npm", "start" ]

### tag = web-client
###  docker run -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm web-client